
var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.use(express.static(__dirname + '/build/default'))

app.listen(port);

console.log('Ejecutando polymer desde node on: ' + port);

app.get('/', function(req, res) {
  //res.send('Hola mundo node js');
  res.sendFile("index.html", {root: '.'});
})
